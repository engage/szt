import Vue from 'vue'
import Router from 'vue-router'

import Index from '@/pages/index'
import Login from '@/pages/login'
import Register from '@/pages/register'
import ShopCar from '@/pages/shopCar'
import Settle from '@/pages/settle'
import AddAddress from '@/pages/addAddress'
import Order from '@/pages/order'
import Set from '@/pages/set'
import Cooperation from '@/pages/cooperation'
import MyAccount from '@/pages/myAccount'
import ManageAddress from '@/pages/manageAddress'
import BankInfo from '@/pages/bankInfo'
import PersonalInfo from '@/pages/personalInfo'
import ApplyShop from '@/pages/applyShop'
import Withdrawals from '@/pages/withdrawals'
import Reward from '@/pages/reward'
import EditAddress from '@/pages/editAddress'
import ModifyPassword from '@/pages/modifyPassword'
import RetrievePassword from '@/pages/retrievePassword'
import EvaluateOrder from '@/pages/evaluateOrder'
import EvaluateList from '@/pages/evaluateList'
import About from '@/pages/about'
import Balance from '@/pages/balance'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/shopcar',
      name: 'ShopCar',
      component: ShopCar
    },
    {
      path: '/settle',
      name: 'Settle',
      component: Settle
    },
    {
      path: '/addAddress',
      name: 'AddAddress',
      component: AddAddress
    },
    {
      path: '/order',
      name: 'Order',
      component: Order
    },
    {
      path: '/set',
      name: 'Set',
      component: Set
    },
    {
      path: '/cooperation',
      name: 'Cooperation',
      component: Cooperation
    },
    {
      path: '/myAccount',
      name: 'MyAccount',
      component: MyAccount
    },
    {
      path: '/manageAddress',
      name: 'ManageAddress',
      component: ManageAddress
    },
    {
      path: '/bankInfo',
      name: 'BankInfo',
      component: BankInfo
    },
    {
      path: '/personalInfo',
      name: 'PersonalInfo',
      component: PersonalInfo
    },
    {
      path: '/applyShop',
      name: 'ApplyShop',
      component: ApplyShop
    },
    {
      path: '/withdrawals',
      name: 'Withdrawals',
      component: Withdrawals
    },
    {
      path: '/reward',
      name: 'Reward',
      component: Reward
    },
    {
      path: '/editAddress',
      name: 'EditAddress',
      component: EditAddress
    },
    {
      path: '/modifyPassword',
      name: 'ModifyPassword',
      component: ModifyPassword
    },
    {
      path: '/retrievePassword',
      name: 'RetrievePassword',
      component: RetrievePassword
    },
    {
      path: '/evaluateOrder',
      name: 'EvaluateOrder',
      component: EvaluateOrder
    },
    {
      path: '/evaluateList',
      name: 'EvaluateList',
      component: EvaluateList
    },
    {
      path: '/about',
      name: 'About',
      component: About
    },
    {
      path: '/balance',
      name: 'Balance',
      component: Balance
    }

  ]
})
